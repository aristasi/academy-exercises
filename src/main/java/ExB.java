
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExB {

    String str = new String("Aris");

    public static String rev (String inp){
        char[] st = inp.toCharArray();
        char[] test = new char[st.length];
        for (int i = 0, j = test.length -1; i < st.length; i++, j-- ) {
            test[j] = st[i];
        }
        String reverse = String.valueOf(test);
        return reverse;
    }

    public static boolean sides (String inp){
        boolean flag = true;

        for (int i = 0; i <inp.length()/2; i++) {
            for (int j = inp.length()-1; j > inp.length()/2; j--)
                if (inp.charAt(i) == inp.charAt(j)){
                    flag = true;
                } else {
                    flag = false;
                }

        }
        System.out.println(flag);
        return flag;

    }

    public static int hammingDist(String str1, String str2)
    {
        int i = 0, count = 0;
        while (i < str1.length())
        {
            if (str1.charAt(i) != str2.charAt(i))
                count++;
            i++;
        }
        return count;
    }

    public static double frequency (String inp, String word){

        double totalWords = inp.split("\\w+").length ;
        System.out.println(totalWords);

        double countOfword = Arrays.stream(inp.split("[ ,\\.]")).filter(s -> s.equals(word)).count();
        System.out.println(countOfword);

        System.out.println(countOfword/totalWords);
        return countOfword/totalWords;


    }

    public static void  freq (String sentence, String seq){
        String s = "hello hellohello test";
        String sequence = "hello";
        int count2 = (s.length() - s.replace(sequence, "").length()) / sequence.length();
        System.out.println(count2);
    }

    public static String result (String inp){
        String res = inp.replaceAll("\\s+", " ");
        System.out.println(res);
        return res;
    }

    public static int BinaryToDecimal(int binaryNumber){

        int decimal = 0;
        int p = 0;
        while(true){
            if(binaryNumber == 0){
                break;
            } else {
                int temp = binaryNumber%10;
                decimal += temp*Math.pow(2, p);
                binaryNumber = binaryNumber/10;
                p++;
            }
        }
        return decimal;
    }

    public static void replaceSubstring(String word, String substring, int position){
        if ((substring.length()+position) <= word.length()) {
            String result = word.substring(0,position) + substring + word.substring(position+substring.length());
            System.out.println(result);
        } else {
            System.out.println("Not valid substring");
        }
    }
    public static void passwordEval(String password){
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
        System.out.println(password.matches(pattern));
    }

    static int numberOfCoins(float amount) {
        int numOfCoins = 0;

        numOfCoins += amount / 2.0;
        amount = amount % 2.0f;

        numOfCoins += amount / 1.0;
        amount = amount % 1.0f;

        numOfCoins += amount / 0.5;
        amount = amount % 0.5f;

        numOfCoins += amount / 0.2;
        amount = amount % 0.2f;

        numOfCoins += amount / 0.1;
        amount = amount % 0.1f;

        numOfCoins += amount / 0.05;
        amount = amount % 0.05f;

        numOfCoins +=  amount / 0.02;
        amount = amount % 0.02f;

        numOfCoins += amount / 0.01;
        amount = amount % 0.01f;

        return numOfCoins;
    }

    // AAAB AABB (
    public static boolean checkAnagram(String stringOne , String stringTwo){
        char[] first = stringOne.toLowerCase().replace(" ", "").toCharArray();
        char[] second = stringTwo.toLowerCase().replace(" ", "").toCharArray();

        if (first.length != second.length) {
            return false;
        } else {
            Set<Character> oneSet = new HashSet<Character>();
            Set<Character>  twoSet = new HashSet<Character> ();

            for (char c : first) { oneSet.add (c); }

            for (char c : second) { twoSet.add (c); }

            return oneSet.equals (twoSet);
        }

    }


//    public static int matchesPolicy(String pwd) {
//        int count = 0;
//        boolean flag = false;
//        if (pwd.length() >= 8)
//            count += 1;
//        if (pwd.equals("?=.*[0-9]"))
//            count += 1;
//        if (pwd.equals("?=.*[a-z]"))
//            count += 1;
//        if (pwd.equals("?=.*[A-Z]"))
//            count += 1;
//        if (pwd.equals("?=.*[@#$%^&+=]"))
//            count += 1;
//            flag = true;
//        if (pwd.equals("?=\\S+$"))
//            count += 1;
//        if ((count >=3) && flag)
//            System.out.println("Password OK");
//        if ((count >=5) && flag)
//            System.out.println("Strong Password");
//        if ((count =6) && flag)
//            System.out.println("Very Strong Password");
//    }



}
